package stpd

import (
	"errors"
	"fmt"
)

type Stupid struct {
	Number int
}

func New(number int) (*Stupid, error) {
	if number < 0 {
		return nil, errors.New("Value should be greater than zero")
	}

	return &Stupid{Number: number}, nil
}

func (s *Stupid) print() {
	fmt.Printf("%v", s)
}

func (s *Stupid) add(a int) (int) {
	s.Number += a
	return s.Number
}